package com.sda.employee.Controller;

import com.sda.employee.exception.EmployeeNotFoundException;
import com.sda.employee.model.Employee;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

public class ControllerAdvisor extends ResponseEntityExceptionHandler {

    public ResponseEntity<Object> handleEmployeeNotFoundException(EmployeeNotFoundException exception) {
        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timeStamp", LocalDateTime.now());
        body.put("message", exception.getLocalizedMessage());

        return new ResponseEntity<>(body, HttpStatus.NOT_FOUND);
    }
}
