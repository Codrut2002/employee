package com.sda.employee.Controller;

import com.sda.employee.components.CustomFakerEmployee;
import com.sda.employee.exception.EmployeeNotFoundException;
import com.sda.employee.model.Employee;
import com.sda.employee.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/employees")
@ControllerAdvice
public class EmployeeController {

    private final EmployeeService employeeService;
    private final CustomFakerEmployee customFakerEmployee;

    @Autowired
    public EmployeeController(EmployeeService employeeService, CustomFakerEmployee customFakerEmployee) {
        this.employeeService = employeeService;
        this.customFakerEmployee = customFakerEmployee;
    }

    @PostMapping("/create")
    public ResponseEntity<Employee> create(@RequestBody Employee employee) {
        return ResponseEntity.ok(employeeService.create(employee));
    }

    @GetMapping("/getAll")
    public ResponseEntity<List<Employee>> getAll() {
        return ResponseEntity.ok(employeeService.getAll());
    }

    @GetMapping("/getAllPaginated")
    public ResponseEntity<List<Employee>> getAllPaginated(
            @RequestParam(defaultValue = "0") Integer pageNumber,
            @RequestParam(defaultValue = "50") Integer pageSize,
            @RequestParam(defaultValue = "name") String sortBy) {
        return ResponseEntity.ok(employeeService.getAllPaginated(pageNumber, pageSize, sortBy));
    }

    @GetMapping("/findByName")
    public ResponseEntity<Employee> findByName(@RequestParam String name) {
        return ResponseEntity.ok(employeeService.findByName(name)
                .orElseThrow(() -> new EmployeeNotFoundException("Employee with name <" + name + "> not found")));
    }

    @GetMapping("/populate")
    public ResponseEntity<String> populate() {
        return ResponseEntity.ok(employeeService.populate(customFakerEmployee.createDummyEmployeeList()));
    }
}
