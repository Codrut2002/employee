package com.sda.employee.service;

import com.sda.employee.model.Employee;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

public interface EmployeeService {

    Employee create(Employee employee);

    String populate(List<Employee> employeeList);

    List<Employee> getAll();

    List<Employee> getAllPaginated(Integer pageNumber, Integer pageSize, String sortBy);

    Optional<Employee> findByName(String name);
}
