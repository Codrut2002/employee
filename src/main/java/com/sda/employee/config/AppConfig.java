package com.sda.employee.config;

import com.sda.employee.components.CustomFakerEmployee;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {

    @Bean
    public CustomFakerEmployee customFakerEmployee() {
        return new CustomFakerEmployee();
    }
}
