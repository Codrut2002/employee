package com.sda.employee.components;

import com.github.javafaker.Faker;
import com.sda.employee.model.Employee;

import java.util.ArrayList;
import java.util.List;

public class CustomFakerEmployee {

    public List<Employee> createDummyEmployeeList() {
        Faker faker = new Faker();
        List<Employee> employeeList = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            Employee employee = new Employee();
            employee.setFirstName(faker.name().firstName());
            employee.setLastName(faker.name().lastName());
            employee.setAddress(faker.address().fullAddress());
            employee.setPhoneNumber(faker.phoneNumber().phoneNumber());
            employee.setEmail(faker.bothify("?????##@yahoo.com"));
            employee.setPersonalNumericCode(faker.number().numberBetween(13L, 13L));
            employee.setHired(faker.bool().bool());

            employeeList.add(employee);
        }
        return employeeList;
    }
}
