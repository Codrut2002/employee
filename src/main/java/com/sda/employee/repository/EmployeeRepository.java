package com.sda.employee.repository;

import com.sda.employee.model.Employee;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface EmployeeRepository extends PagingAndSortingRepository<Employee, Integer> {

    Optional<Employee> findByName(String name);

}
